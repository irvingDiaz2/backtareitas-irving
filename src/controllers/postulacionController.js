const { pool } = require("../database/config");

//Postulación a un proyecto
const create = async (req, res) => {
    return new Promise((resolve, reject) => {
        pool.query('INSERT INTO postulaciones set ?', [req.body]);
        console.log(req.body);
        res.json({
            ok: true,
            text: 'Postulación creada',
            data: req.body
        });
    });
}

//Proyectos a los que se esta postulado
const getAll = async (req,res) => {
    const { id } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM postulaciones inner join proyectos on postulaciones.id_proyecto=proyectos.id WHERE proyectos.activo = 1 AND postulaciones.activo = 1 AND id_user = ?',[id],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                    res.json({
                        ok: true,
                        text: 'Proyectos a los que esta postulado',
                        data: rows
                    });
            }else{
                res.json({
                    ok: true,
                    text: 'No tiene proyectos postulados',
                    data: rows
                });
                }
        });
    });
}

//Eliminar una postulación
const desactivar = async (req, res) => {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE postulaciones set activo = 0 WHERE id_proyecto = ? AND id_user = ?',[req.body.id_proyecto, req.body.id_user],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text: 'Usted se ha salido de este proyecto',
                    data: req.body
                });
        });
    });
}

//Volver a activarse
const deNuevo = async (req, res) => {
    return new Promise((resolve, reject) => {
        pool.query('UPDATE postulaciones set activo = 1 WHERE id_proyecto = ? AND id_user = ? AND activo = 0',[req.body.id_proyecto, req.body.id_user],
            (err, rows) => {
                if (err) reject(err.message);
                res.json({
                    ok: true,
                    text: 'Usted ha volvido ha este proyecto',
                    data: req.body
                });
        });
    });
}


//Proyectos de los que se ha salido
const getAllExit = async (req,res) => {
    const { id } = req.params;
    return new Promise((resolve, reject) => {
        pool.query('SELECT * FROM postulaciones inner join proyectos on postulaciones.id_proyecto=proyectos.id WHERE proyectos.activo = 1 AND postulaciones.activo = 0 AND id_user = ?',[id],
        (err, rows) => {
            if (err) reject(err.message);
                if(rows.length>0){
                    res.json({
                        ok: true,
                        text: 'Proyectos de los que se ha salido',
                        data: rows
                    });
            }else{
                res.json({
                    ok: true,
                    text: 'No se ha salido de ningun proyecto',
                    data: rows
                });
                }
        });
    });
}

module.exports = {
    create,
    getAll,
    desactivar, 
    deNuevo,
    getAllExit
}